Mapa online Politechniki Łódzkiej

Online:

https://pooler22.github.io/MapaPL/index.html

Jestem studentem PŁ i w wolnej chwili napisałem taką aplikację. Proszę o wszystkie uwagi opinie itp.

Wersja es6 dostępna z poziomu pliku test.html, który korzysta z script_over_es2015.js

Wersja na starsze przeglądarki dostepna z poziomu pliku index.html, który korzysta z script.js

Plik script_over_es2015.js jest transpilowany do script.js
